### restart-docker.sh  
  
Script that
* Checks that there are no more than 50000 sockets opened in the system (`lsof | grep docker.sock`)
* If that is the case, checks whether there are docker containers running (`docker ps`)
* If there is not containers runnung, restart docker (`systemctl restart docker.service`)
* Otherwise, sleep 1 minute (second try) then 5 minutes (thrid try)
* After the thrid try, give up till next run of the cron job
  
To be added as a cronjob executed every 15 minutes
